package com.yoav.chat.client;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class Client {
	
	private final String ADDRESS = "127.0.0.1";
	private final int PORT = 5555;

	
	
	public void init(){
		
		try {
			InetSocketAddress address = new InetSocketAddress(ADDRESS, PORT);
			SocketChannel clientSocketChannel = SocketChannel.open(address);
			String textLine = null;
			Console c = System.console();
	        
			if (c == null) {
	            System.err.println("No console.");
	            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
	            textLine = reader.readLine();
	         //   System.exit(1);
	        }
			System.out.println("please write things to chat server - write 'quit!' when you are tired ");
	        
			while(!(textLine = c.readLine("::")).equals("quit!")){
				System.out.println(textLine);
	        	byte[] line = textLine.getBytes();
	        	ByteBuffer messageByteBuffer = ByteBuffer.wrap(line);
	        	clientSocketChannel.write(messageByteBuffer);
	        	messageByteBuffer.clear();
			}
			
	        
			
			System.out.println("bye bye!");
			clientSocketChannel.close();			
			System.exit(0);
	        
		}catch (BindException e) {
			System.out.println("the selected socket is in use");
			
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	
	
}
