package com.yoav.chat.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public enum Server {
	INSTANCE;
	
	private static final String ADDRESS = "127.0.0.1";
	private static final int PORT = 5555;
	private static final int TIMEOUT = 10000;
	
	private static ServerSocketChannel sschannel;
    private static Selector selector;

    /**
     * init the server for getting the user 'chat messages' displayed
     */
	public static void init(){
		 System.out.println("Init the server...accepting connections");
		try{
			
			 ServerSocketChannel sschannel = ServerSocketChannel.open();
			 selector = Selector.open();			
			 sschannel.configureBlocking(false);
			 sschannel.socket().bind(new InetSocketAddress(ADDRESS,PORT));
			 sschannel.register(selector,SelectionKey.OP_ACCEPT);
			 
			 
			 while(!Thread.currentThread().isInterrupted()){		

					 if(selector.select(TIMEOUT) == 0)
						 continue;
					 
				      
				      Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
				      while (iterator.hasNext()) {
					        SelectionKey key = iterator.next();
					        iterator.remove();
					        if (!key.isValid()){
		                        continue;
		                    }
					        if (key.isAcceptable()) {
					        	
						        SocketChannel client = sschannel.accept();
						        System.out.println("Accepted connection from " + client.getRemoteAddress());
						        client.configureBlocking(false);
						        client.register(selector, SelectionKey.OP_READ);					          
					         
					        } 
					         else if (key.isReadable()) {
					        	 
					          SocketChannel client = (SocketChannel) key.channel();
					          
					        
					          ByteBuffer output = ByteBuffer.allocate(256);
					          
					          try{int readVal = client.read(output);
						         
						          if(readVal <= 0 )
						        	  continue;
						          System.out.println(client.getLocalAddress()+":: "+ new String(output.array()).trim());
						         
						          output.flip();				          
					          }
					          catch (IOException e) {
					        	System.out.println("Connection was closed on the user " +client.getRemoteAddress());  
								client.close();
					          }
					          
					          
					        }	        
				        
				      	}				
			 }
		
		}
		catch (IOException e) {
			
			e.printStackTrace();
		}
		finally {
			closeConnection();
		}
		
		
		 
	}
	
	private static void closeConnection(){
        System.out.println("Closing server down");
        if (selector != null){
            try {
                selector.close();
                sschannel.socket().close();
                sschannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
		
}
